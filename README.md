# AlgoCasts

Companion repo to a course on Udemy.com


`npm i` to install Jest locally 

and 

`npx jest exercises/<whatever-the-exercise>/test.js` to run the tests

Use `--watch` argument to re-run the tests on change

NOTE: better to install Jest globally `npm i -g jest` to make runs easier (no `npx` needed)
