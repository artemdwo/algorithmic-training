const reverseInt = require('./index');

test('ReverseInt function exists', () => {
  expect(reverseInt).toBeDefined();
});

test('ReverseInt handles 0 as an input', () => {
  expect(reverseInt(0)).toEqual(0);
  expect(reverseInt(-0)).toEqual(-0);
});

test('ReverseInt flips a positive number', () => {
  expect(reverseInt(5)).toEqual(5);
  expect(reverseInt(15)).toEqual(51);
  expect(reverseInt(90)).toEqual(9);
  expect(reverseInt(2359)).toEqual(9532);
  expect(reverseInt(200359)).toEqual(953002);
  expect(reverseInt(230059)).toEqual(950032);
  expect(reverseInt(235009)).toEqual(900532);
  expect(reverseInt(23500900)).toEqual(900532);
});

test('ReverseInt flips a negative number', () => {
  expect(reverseInt(-5)).toEqual(-5);
  expect(reverseInt(-15)).toEqual(-51);
  expect(reverseInt(-90)).toEqual(-9);
  expect(reverseInt(-2359)).toEqual(-9532);
  expect(reverseInt(-200359)).toEqual(-953002);
  expect(reverseInt(-230059)).toEqual(-950032);
  expect(reverseInt(-235009)).toEqual(-900532);
  expect(reverseInt(-23500900)).toEqual(-900532);
});
