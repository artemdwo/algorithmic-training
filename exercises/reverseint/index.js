// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
  // SHORT
  return Number(n.toString().replace('-','').split("").reverse().join("")) *  Math.sign(n)

  // SHORT Alternative with parseInt() method
  // return parseInt(n.toString().replace('-','').split("").reverse().join("")) *  Math.sign(n)

  // LONG
  /**
   * Conver Number into String
   * let str = n.toString();
   * 
   * REmove '-' sign
   * str = str.replace('-','')
   * 
   * Conver into Array
   * let arr = str.split("");
   * 
   * Reverse the Array
   * arr.reverse();
   * 
   * Convert the reversed version back to String
   * let str_r = arr.join("");
   * 
   * Multiply the reversed Number by the result of
   * sign() method that gives 1 or -1 (0 or -0) 
   * based on input Number
   * let result = Number(str_r) * Math.sign(n)
   * 
   * Return the number
   * return result
   */
}

module.exports = reverseInt;
