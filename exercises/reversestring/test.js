const { reverse, reverse0, reverse00, reverse1, reverse2 } = require("./index");

describe("Reverse", () => {
  it("exists", () => {
    expect(reverse).toBeDefined();
  });

  it("reverses a string A", () => {
    expect(reverse("abcd")).toEqual("dcba");
  });

  it("reverses a string B", () => {
    expect(reverse("  abcd")).toEqual("dcba  ");
  });
});

// Reverse 0
describe("Reverse 0", () => {
  it("exists", () => {
    expect(reverse0).toBeDefined();
  });

  it("reverses a string A", () => {
    expect(reverse0("abcd")).toEqual("dcba");
  });

  it("reverses a string B", () => {
    expect(reverse0("  abcd")).toEqual("dcba  ");
  });
});

// Reverse 00
describe("Reverse 00", () => {
  it("exists", () => {
    expect(reverse00).toBeDefined();
  });

  it("reverses a string A", () => {
    expect(reverse00("abcd")).toEqual("dcba");
  });

  it("reverses a string B", () => {
    expect(reverse00("  abcd")).toEqual("dcba  ");
  });
});

// Reverse 1
describe("Reverse 1", () => {
  it("exists", () => {
    expect(reverse1).toBeDefined();
  });

  it("reverses a string A", () => {
    expect(reverse1("abcd")).toEqual("dcba");
  });

  it("reverses a string B", () => {
    expect(reverse1("  abcd")).toEqual("dcba  ");
  });
});

// Reverse 2
describe("Reverse 2", () => {
  it("exists", () => {
    expect(reverse2).toBeDefined();
  });

  it("reverses a string A", () => {
    expect(reverse2("abcd")).toEqual("dcba");
  });

  it("reverses a string B", () => {
    expect(reverse2("  abcd")).toEqual("dcba  ");
  });
});