// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

// The most advanced and interesting solution
// using Array reducer "array.reduce()"
function reverse(str) {
  // SHORT
  return str.split("").reduce((rev, char) => char + rev, "");

  // The same but LONG
  // return str.split("").reduce((reversed, character) => {
  //   return character + reversed;
  // }, "");
}

// Not very trivial one - unusual
function reverse0(str) {
  return str.split("").reverse().toString().replace(/,/gi, "");
}

// Very trivial solution
function reverse00(str) {
  return str.split("").reverse().join("");
}

// Old-school FOR loop
// Not recommended when there is a need to loop through
// every element as introduces many point to make a mistake
function reverse1(str) {
  let out = "";
  for (let i = str.length - 1; i >= 0; i--) {
    out = out + str[i];
  }
  return out;
}

// Modern FOR loop (ES2015)
// Easier to use when iterates through every element
function reverse2(str) {
  let reversed = "";
  for (let character of str) {
    reversed = character + reversed;
  }
  return reversed;
}

module.exports = { reverse, reverse0, reverse00, reverse1, reverse2 };
