// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false

// The most interesting solution
/**  
 * Though, isn't the most optimal because of
 * excessive comparison:
 * 
 * array.every() will compare every element (from the beginning)
 * with its mirror on the other side of the array
 * and then do the same with the elements on array's end:
 * 0 -> 4
 * 1 -> 3
 * 2 -> 2
 * 3 -> 1
 * 4 -> 0
 */ 
function palindrome(str) {
  // SHORT
  return str.split("").every((char, index) => char === str[str.length - index - 1]);

  // LONG
  // return str.split("").every((char, index) => {
  //   return char === str[str.length - index - 1];
  // });
}

module.exports = palindrome;

// Trivial solution
function palindrome_trivial(str) {
  return str.split("").reverse().join("") === str ? true : false;

  // Details
  /**
   * Convert string into array
   * let arr = str.split("")
   *
   * Reverse the array
   * arr.reverse()
   *
   * Convert the array into string
   * const reversed = arr.join("")
   *
   * Return TRUE if 'reversed' equals 'str'
   * and FALSE otherwise
   * return reversed === str
   */
}
