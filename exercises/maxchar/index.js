// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

function maxChar(str) {
  let chars = {};

  // SHORT
  // NOTE: "for ... of" is used for iterable objects
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
  for (let char of str) {
    chars[char] = chars[char] + 1 || 1;
  }

  // LONG
  // for (let char of str) {
  //   if (chars[char]) {
  //     chars[char]++;
  //   } else {
  //     chars[char] = 1;
  //   }
  // }

  let max = 0;
  let theChar = "";

  // NOTE: "for ... in" is used for objects (enumerable properties of an object)
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in
  for (let char in chars) {
    if (chars[char] > max) {
      max = chars[char];
      theChar = char;
    }
  }

  return theChar;
}

module.exports = maxChar;
